import os
from flask import Flask
from cloud_utils import gcp_utils, aws_utils

app = Flask("cloud_operator")

@app.route("/")
def healthCheck():
    return {"status": f'Cloud_Operator application is up and running on {os.getenv("CURRENT_ENV", "undefined")} environment', "status_code": 200}

if __name__=="__main__":
    app.run(host="0.0.0.0", port=8080)