from setuptools import setup 
import json

setup( 
	name='cloud_operator', 
	version='0.1', 
	description='Cloud Operations', 
	author='Gunasekar Velraj', 
	author_email='karthiguna065150@gmail.com', 
	packages=['cloud_operator'], 
	install_requires=["flask"]
) 
